package main

import (
	"log"
	"net/http"
)

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	log.Println("Listening on 0.0.0.0:1337")
	if err := http.ListenAndServe("0.0.0.0:1337", nil); err != nil {
		log.Fatal(err)
	}
}
