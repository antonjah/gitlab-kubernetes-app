FROM golang:1.18-alpine

WORKDIR /app
COPY cmd .
COPY go.mod .
RUN CGO_ENABLED=0 go build -o gitlab-kubernetes-app ./...

FROM scratch

COPY --from=0 ./app/gitlab-kubernetes-app .
COPY static ./static

EXPOSE 1337

CMD ["./gitlab-kubernetes-app"]